package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next().doubleValue();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public double getMax(IntegersBag bag){
		double max = Integer.MIN_VALUE;
	    double value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next().doubleValue();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public double getMin(IntegersBag bag){
		double min = Integer.MAX_VALUE;
	    double value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next().doubleValue();
				if( min > value){
					min= value;
				}
			}
			
		}
		return min;
	} 
	
	public String getEvenNumbers(IntegersBag bag )
	{
		String anw="";
		double value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next().doubleValue();
				if( value%2 == 0){
					anw += value +" // " ;
				}
			}
			
		}
		return anw;
	}
	
	public String getPrimesNumbers(IntegersBag bag )
	{
		String anw="";
		int c,d;
		double value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next().doubleValue();
				d = 1;
				for (c=0;d<=value ;d++)
				{
					if((value%d)==0){
						c++;	
					}
					
				}
				if(c == 2){
					anw += value +" // " ;
				}
			}
			
		}
		return anw;
	}
	
}
